### A Pluto.jl notebook ###
# v0.18.0

using Markdown
using InteractiveUtils

# ╔═╡ 1e26cc08-cd76-4108-a0ed-28bc46d2de95
using Unitful, 
	CommonMark, 
	LaTeXStrings

# ╔═╡ e15a463e-8d87-11ec-23b7-8950f5cd0c1c


# ╔═╡ 66d1d400-a58b-4623-838d-88fc960511ec


# ╔═╡ bde82185-3f3e-4633-849f-f1d65f063c11
cm"""
2. **(ms6,x2.4,adopted)** During steady-state operation, a gearbox receives 60 kW through 
   the input shaft and delivers power through the
   output shaft. For the gearbox as the system, the rate of energy transfer by convection is
   ```math
   \require{physics}
   \begin{equation}
   \dot Q_{out} = hA \left( T_b - T_\infty \right)
   \end{equation}
   ```
   
   ``\qquad`` where
   
	```math
   \begin{align*}
        \small h &=\small \pu{0.171 kW/m^2 \cdot K} \small\textsf{ is the heat transfer coefficient;} \\
        A &=\small \pu{1.0m} \small\textsf{ is the outer surface area of the gearbo,} \\
        T_b &= \small\pu{27°C} \small\textsf{ is the temperature at the outer surface; and} \\
        T_\infty &= \small\pu{20°C} \small\textsf{ is the ambient temperature of the air.}
   \end{align*}
   ```
"""

# ╔═╡ 58c14931-ff78-4a25-883e-4eb1dff0a15f
cm"""
## Get organized

Assume
- frictionless piston: no info given

```math
\begin{gather}
\dot E_{in} &-& \dot E_{out} &=& \dv t E_{sys} \\[6pt]
\dot Q_{in} &-& \dot W_{out} &=& \dv t U_{sys} \\[6pt]
\dot Q_{out} &+& W_{s,in}  &=& 0 \\[6pt]
Q_{in} &+& W_{pw,in} - W_{b,out} &=& m \left( u_2 - u_1 \right) \\[6pt]{}
\end{gather}
```

So,

```math
 W_{b,out} = Q_{in} + W_{pw,in} - m \left( u_2 - u_1 \right) \\
```
"""

# ╔═╡ 77578c8d-65b1-409f-9501-0364781e3801


# ╔═╡ ed727ea8-645f-4a9f-a3a9-dd428ad70563


# ╔═╡ 97e9a4ef-f798-4f55-9c1c-4065ce33fe8b


# ╔═╡ 747a64d8-c20a-4577-81df-6c74348e202a


# ╔═╡ 00000000-0000-0000-0000-000000000001
PLUTO_PROJECT_TOML_CONTENTS = """
[deps]
CommonMark = "a80b9123-70ca-4bc0-993e-6e3bcb318db6"
LaTeXStrings = "b964fa9f-0449-5b57-a5c2-d3ea65f4040f"
Unitful = "1986cc42-f94f-5a68-af5c-568840ba703d"

[compat]
CommonMark = "~0.8.6"
LaTeXStrings = "~1.3.0"
Unitful = "~1.11.0"
"""

# ╔═╡ 00000000-0000-0000-0000-000000000002
PLUTO_MANIFEST_TOML_CONTENTS = """
# This file is machine-generated - editing it directly is not advised

julia_version = "1.7.1"
manifest_format = "2.0"

[[deps.Artifacts]]
uuid = "56f22d72-fd6d-98f1-02f0-08ddc0907c33"

[[deps.CommonMark]]
deps = ["Crayons", "JSON", "URIs"]
git-tree-sha1 = "4cd7063c9bdebdbd55ede1af70f3c2f48fab4215"
uuid = "a80b9123-70ca-4bc0-993e-6e3bcb318db6"
version = "0.8.6"

[[deps.CompilerSupportLibraries_jll]]
deps = ["Artifacts", "Libdl"]
uuid = "e66e0078-7015-5450-92f7-15fbd957f2ae"

[[deps.ConstructionBase]]
deps = ["LinearAlgebra"]
git-tree-sha1 = "f74e9d5388b8620b4cee35d4c5a618dd4dc547f4"
uuid = "187b0558-2788-49d3-abe0-74a17ed4e7c9"
version = "1.3.0"

[[deps.Crayons]]
git-tree-sha1 = "249fe38abf76d48563e2f4556bebd215aa317e15"
uuid = "a8cc5b0e-0ffa-5ad4-8c14-923d3ee1735f"
version = "4.1.1"

[[deps.Dates]]
deps = ["Printf"]
uuid = "ade2ca70-3891-5945-98fb-dc099432e06a"

[[deps.JSON]]
deps = ["Dates", "Mmap", "Parsers", "Unicode"]
git-tree-sha1 = "3c837543ddb02250ef42f4738347454f95079d4e"
uuid = "682c06a0-de6a-54ab-a142-c8b1cf79cde6"
version = "0.21.3"

[[deps.LaTeXStrings]]
git-tree-sha1 = "f2355693d6778a178ade15952b7ac47a4ff97996"
uuid = "b964fa9f-0449-5b57-a5c2-d3ea65f4040f"
version = "1.3.0"

[[deps.Libdl]]
uuid = "8f399da3-3557-5675-b5ff-fb832c97cbdb"

[[deps.LinearAlgebra]]
deps = ["Libdl", "libblastrampoline_jll"]
uuid = "37e2e46d-f89d-539d-b4ee-838fcccc9c8e"

[[deps.Mmap]]
uuid = "a63ad114-7e13-5084-954f-fe012c677804"

[[deps.OpenBLAS_jll]]
deps = ["Artifacts", "CompilerSupportLibraries_jll", "Libdl"]
uuid = "4536629a-c528-5b80-bd46-f80d51c5b363"

[[deps.Parsers]]
deps = ["Dates"]
git-tree-sha1 = "13468f237353112a01b2d6b32f3d0f80219944aa"
uuid = "69de0a69-1ddd-5017-9359-2bf0b02dc9f0"
version = "2.2.2"

[[deps.Printf]]
deps = ["Unicode"]
uuid = "de0858da-6303-5e67-8744-51eddeeeb8d7"

[[deps.Random]]
deps = ["SHA", "Serialization"]
uuid = "9a3f8284-a2c9-5f02-9a11-845980a1fd5c"

[[deps.SHA]]
uuid = "ea8e919c-243c-51af-8825-aaa63cd721ce"

[[deps.Serialization]]
uuid = "9e88b42a-f829-5b0c-bbe9-9e923198166b"

[[deps.URIs]]
git-tree-sha1 = "97bbe755a53fe859669cd907f2d96aee8d2c1355"
uuid = "5c2747f8-b7ea-4ff2-ba2e-563bfd36b1d4"
version = "1.3.0"

[[deps.Unicode]]
uuid = "4ec0a83e-493e-50e2-b9ac-8f72acf5a8f5"

[[deps.Unitful]]
deps = ["ConstructionBase", "Dates", "LinearAlgebra", "Random"]
git-tree-sha1 = "b649200e887a487468b71821e2644382699f1b0f"
uuid = "1986cc42-f94f-5a68-af5c-568840ba703d"
version = "1.11.0"

[[deps.libblastrampoline_jll]]
deps = ["Artifacts", "Libdl", "OpenBLAS_jll"]
uuid = "8e850b90-86db-534c-a0d3-1478176c7d93"
"""

# ╔═╡ Cell order:
# ╠═e15a463e-8d87-11ec-23b7-8950f5cd0c1c
# ╠═1e26cc08-cd76-4108-a0ed-28bc46d2de95
# ╠═66d1d400-a58b-4623-838d-88fc960511ec
# ╠═bde82185-3f3e-4633-849f-f1d65f063c11
# ╠═58c14931-ff78-4a25-883e-4eb1dff0a15f
# ╠═77578c8d-65b1-409f-9501-0364781e3801
# ╠═ed727ea8-645f-4a9f-a3a9-dd428ad70563
# ╠═97e9a4ef-f798-4f55-9c1c-4065ce33fe8b
# ╠═747a64d8-c20a-4577-81df-6c74348e202a
# ╟─00000000-0000-0000-0000-000000000001
# ╟─00000000-0000-0000-0000-000000000002
